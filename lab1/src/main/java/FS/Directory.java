package FS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Directory extends Entity {
    private static final int DIR_MAX_ELEMS = 25;
    private Map<String, Entity> children = new HashMap<>(DIR_MAX_ELEMS);
    private final Semaphore semaphore;
    private int countReaders;

    private Directory() {
        super("root");
        semaphore = new Semaphore(1);
    }

    private Directory(String name, Directory parent) {
        super(name, parent);
        semaphore = new Semaphore(1);
    }

    protected void atomicSynchronizedOperationWithChildren(Runnable fn) {
        semaphore.acquireUninterruptibly();
        try {
            fn.run();
        } finally {
            semaphore.release();
        }
    }

    protected void acquireChildren() {
        semaphore.acquireUninterruptibly();
    }

    protected void releaseChildren() {
        semaphore.release();
    }

    protected <T> T atomicSynchronizedOperationWithTree(Supplier<T> fn) {
        forEachAncestors(Directory::acquireChildren);
        try {
            return fn.get();
        } finally {
            forEachAncestors(Directory::releaseChildren);
        }
    }

    private void forEachAncestors(Consumer<Directory> fn) {
        Directory cur = this;
        while (cur != null) {
            fn.accept(cur);
            cur = cur.getParent();
        }
    }

    protected boolean containsChild(String childName) {
        return children.containsKey(childName);
    }

    protected int countChildren() {
        return children.size();
    }

    protected Entity getChild(String childName) {
        return children.get(childName);
    }

    protected void addChild(Entity child) {
        if (countChildren() >= DIR_MAX_ELEMS) {
            throw new IllegalCallerException("Too many elements in directory");
        }
        children.put(child.getName(), child);
    }

    protected Entity removeChild(String childName) {
        return children.remove(childName);
    }

    protected void updateChild(Entity child, String oldName) {
        children.put(child.getName(), removeChild(oldName));
    }

    protected List<Entity> getChildren() {
        return new ArrayList<>(children.values());
    }

    private void throwExceptionIfDirIsRoot() {
        if (getParent() == null) {
            throw new IllegalCallerException("Root directory couldn't move");
        }
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory();
        }
        return new Directory(name, parent);
    }

    public List<Entity> list() {
        synchronized (semaphore) {
            if (countReaders++ == 0) {
                semaphore.acquireUninterruptibly();
            }
        }
        try {
            return getChildren();
        } finally {
            synchronized (semaphore) {
                if (--countReaders == 0) {
                    semaphore.release();
                }
            }
        }
    }

    @Override
    public void delete() {
        atomicSynchronizedOperationWithChildren(() -> {
            if (countChildren() != 0) {
                throw new IllegalCallerException("Dir not empty");
            }
            super.delete();
        });
    }

    @Override
    public void move(Directory destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public void move(String destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public EntityType getType() {
        return EntityType.DIRECTORY;
    }

    @Override
    public boolean isDirectory() {
        return true;
    }
}