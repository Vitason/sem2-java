package Threads;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ThreadLocalRandom;

public abstract class thread extends Thread {
    private static int count = 0;
    private int num;
    private static CyclicBarrier cyclicBarrie;
    protected static CountDownLatch countDownLatchCount2 = new CountDownLatch(2);
    protected static CountDownLatch countDownLatchCount4 = new CountDownLatch(4);

    public thread(int num) {
        this.num = num;
        cyclicBarrie = new CyclicBarrier(++count);
    }

    @Override
    public void run() {
        task_init();
        phaseOne();
        task();
        phaseTwo();
        task_finalize();
        phaseThree();
    }

    public void phaseOne() {
        try {
            cyclicBarrie.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    protected abstract void phaseTwo();

    public void phaseThree() {
        countDownLatchCount4.countDown();
    }

    private void simulateWork(String name) {
        System.out.println("thread # " + num + ": started " + name);
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(500, 5001));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("thread # " + num + ": ended " + name);
    }

    public void task_init() {
        simulateWork("task_init");
    }

    public void task() {
        simulateWork("task");
    }

    public void task_finalize() {
        simulateWork("task_finalize");
    }
}
